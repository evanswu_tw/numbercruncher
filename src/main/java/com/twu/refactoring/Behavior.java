package com.twu.refactoring;

public interface Behavior {

    boolean isTrue(int number);

    // as mentioned in the test, we need to keep all the original method here
    // think about what we did in bouncing ball and how we can use that idea to
    // implement number cruncher
}
