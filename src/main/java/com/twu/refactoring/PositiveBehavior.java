package com.twu.refactoring;

public class PositiveBehavior implements Behavior {
    @Override
    public boolean isTrue(int number) {
        return number >= 0;
    }
}
