package com.twu.refactoring;


import java.util.Arrays;

public class NumberCruncher {
    private int[] numbers;

    public NumberCruncher(int... numbers) {
        this.numbers = numbers;
    }


    public int countEven() {
        EvenBehavior even = new EvenBehavior();
        return (int) Arrays.stream(numbers).filter(even::isTrue).count();
    }

    public int countOdd() {
        OddBehavior odd = new OddBehavior();
        return (int) Arrays.stream(numbers).filter(odd::isTrue).count();
    }

    public int countNegative() {
        NegativeBehavior negative = new NegativeBehavior();
        return (int) Arrays.stream(numbers).filter(negative::isTrue).count();
    }

    public int countPositive() {
        PositiveBehavior positive = new PositiveBehavior();
        return (int) Arrays.stream(numbers).filter(positive::isTrue).count();
    }



}
