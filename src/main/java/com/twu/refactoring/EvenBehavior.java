package com.twu.refactoring;

public class EvenBehavior implements Behavior {

    @Override
    public boolean isTrue(int number) {
        return number % 2 == 0;
    }
}
