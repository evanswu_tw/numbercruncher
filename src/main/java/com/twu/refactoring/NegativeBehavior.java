package com.twu.refactoring;

public class NegativeBehavior implements Behavior {
    @Override
    public boolean isTrue(int number) {
        return number < 0;
    }
}
